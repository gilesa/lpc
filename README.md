Slime Attack
=============

A HTML5 tower defense type game.

The princess is stranded and needs protecting from a  
source of slimes nearby. Use your building and magic 
abilities to keep her safe!

 - Source repository: https://bitbucket.org/gilesa/lpc
 
 
Running
--------------------

To play Slime Attack, open 'index.html' in any HTML5 compliant
browser (for example, Firefox, Chrome, Safari, or Internet Explorer 9)


Instructions
--------------------

You must prevent the slimes from reaching the princess. If any 
slime does, you lose the game. Slimes will appear from a hole in 
the ground and blindly wander the landscape. While they will 
relentlessly look for food, they cannot cross water or climb 
over objects. Use a variety of items and magical attacks to slow 
them down and destroy them all before they reach the princess.

Each level is split into three rounds. Each round has a build 
phase and an action phase.

The build phase allows you to build fences. Fences can be 
used to herd slimes away from the princess, though you cannot 
completely wall her off. Fences cost one gold coin each. To 
place a fence, select one from the Wall Menu and click where 
you would like to place it. All fences work and cost the same, 
they just have different looks. When you are finished building, 
click 'Start' to start the action phase.

In the action phase, the slimes will start spawning. You have 
a variety of magic attacks you can use to kill the slimes. 
Each attack has a different damage rate and mana cost. Your mana 
automatically recharges over time, but you won't be able to use 
an attack until you have stored up enough mana for it. You can 
select an attack by clicking on it in the Attack Menu, and use 
it by clicking where you'd like to cast it.

Another item you can use to help defend the princess is bait. 
If you place a bait on the field and a slime finds it, it will 
take a bite and head back to their hole to stash some away. When 
they do this, they will also leave a pheromone trail which will 
lead any other slimes to the fruit. You can select Bait by 
clicking on it in the Item Menu, and use it by clicking where you'd 
like to place it. Each bait will cost you one gold coin.

#### HINTS

- Use `1, 2, 3` on your keyboard to quickly switch between 
magic attacks.
- Some slimes will drop gold coins; make sure to grab them!
- Try attacking large groups of slimes as all caught in the 
attack radius will be hurt.
- Use fences to make the path to the princess much longer.
- Use bait to group slimes together.
- Use bait as a last line of defense before the princess.
- While bait is more effective than fences at grouping 
together slimes, they will disappear once they have been 
fully eaten or when the round ends. You'll need to divide 
your coins between both fences and bait.


Create Mode
--------------------

In this mode, you can design your own maps! Each map must have a 
slime spawn hole and a princess. To play your map right away, 
click 'Start'. To save your map, click 'Save' and copy the 
text that appears. To load a map, click 'Load' and paste in a 
previously saved map.


Future Versions
--------------------

Unfortunately, I ran out of time and did not get to implement 
everything that I wanted to. Below is a list of things I plan 
to add in future versions.

- new enemies, with different attributes and magic weaknesses
- new items and magic attacks
- new wall types, with different attributes
- new terrain types for Create mode
- sound effects
