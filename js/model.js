/*
 * Slime Attack - Liberated Pixel Cup Contest Entry
 * Copyright (C) 2012 Andrew Giles
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 


/*
 * Grid, walls, path
 */
function Grid( width, height, backgroundSprite  )
{
	this.width = width;
	this.height = height;
    this.size = 32; // Each grid unit size, must divide evenly into the width and height
	this.src = { x: -1, y: -1 }; // Source grid square
	this.dst = { x: -1, y: -1 }; // Destination grid square
	this.dstPathLength = Number.POSITIVE_INFINITY; // Length of shortest path from source to destination
	this.dstPath = []; // Grid path slimes will follow
	this.spaces = []; // Double array of each grid space with info about it (wall, distance from source, food)
	this.backgroundSprite = backgroundSprite;
	
	
	/*
	 * Initialize
	 */
	this.init = function( src, dst, map ) {
		// Reset values
		this.dstPathLength = Number.POSITIVE_INFINITY;
		this.dstPath = [];
		this.spaces = [];
				
		// Set source
		if( src )
		{
			this.src.x = src.x;
			this.src.y = src.y;
		}
		else
		{
			this.src.x = -1;
			this.src.y = -1;
		}
		
		// Set destination
		if( dst )
		{
			this.dst.x = dst.x;
			this.dst.y = dst.y;
		}
		else
		{
			this.dst.x = -1;
			this.dst.y = -1;
		}
		
		// Create grid
		var max_width = this.width / this.size; 
		var max_height = this.height / this.size;
		
		// Create grid
		for( var x = 0; x < max_width; x++ )
		{
			this.spaces[x] = [];
				
			for( var y = 0; y < max_height; y++ )
			{
				var wall, background;
				
				// Set wall
				if( map && map[x][y].wall )
				{					
					wall = map[x][y].wall;
				}
				else
				{
					wall = null;
				}
				
				// Set background image
				if( map && map[x][y].background )
				{
					background = map[x][y].background;
				}
				else
				{
					background = this.backgroundSprite.random();
				}
				
				// Create spaces object
				this.spaces[x][y] = {
					wall: wall,
					distance: Number.POSITIVE_INFINITY,
					food: null,
					pheromones: 0,
					background: background
				}
			}
		}
	},


	/*
	 * Resets grid
	 */
	this.reset = function() {
		this.dstPathLength = Number.POSITIVE_INFINITY;
		this.dstPath = [];
		for( var x = 0; x < this.spaces.length; x++ )
		{			
			for( var y = 0; y < this.spaces[x].length; y++ )
			{
				this.spaces[x][y].distance = Number.POSITIVE_INFINITY;
				this.spaces[x][y].food = null;
				this.spaces[x][y].pheromones = 0;
			}
		}
		
		this.initPaths();
	}
	
	/*
	 * Resets the current path
	 */
	this.resetPath = function() {
		this.dstPathLength = Number.POSITIVE_INFINITY;
		this.dstPath = [];
		for( var x = 0; x < this.spaces.length; x++ )
		{			
			for( var y = 0; y < this.spaces[x].length; y++ )
			{
				this.spaces[x][y].distance = Number.POSITIVE_INFINITY;
			}
		}
	}
	
	
	/*
	 * Sets up the grid
	 */
	this.initPaths = function() {
		this.findAllPaths( this.src.x, this.src.y );
		return this.selectPath();
	}
	
	
	/*
	 * Recursivelly finds all paths to a destination
	 *
	 * x - current x coordinate
	 * y - current y coordinate
	 * distance - current distance travelled (optional, defaults to 0)
	 */
	this.findAllPaths = function( x, y, distance ) {
	
		if( distance == null )
		{
			distance = 0;
		}
	
		// Already checked or path has exceeded found path, stop checking
		if( this.spaces[x][y].distance <= distance )
		{
			return;
		}

		// At destination
		if( x == this.dst.x && y == this.dst.y )
		{
			this.dstPathLength = distance;
		}
		
		// Set current position with distance
		this.spaces[x][y].distance = distance;
		distance++;
		
		// Go down
		if( y < this.spaces[x].length-1 && this.spaces[x][y+1].wall == null )
		{
			this.findAllPaths( x, y+1, distance );
		}
		
		// Go right
		if( x < this.spaces.length-1 && this.spaces[x+1][y].wall == null )
		{
			this.findAllPaths( x+1, y, distance );
		}
		
		// Go left
		if( x > 0 && this.spaces[x-1][y].wall == null )
		{
			this.findAllPaths( x-1, y, distance );
		}
		
		// Go up
		if( y > 0 && this.spaces[x][y-1].wall == null )
		{
			this.findAllPaths( x, y-1, distance );
		}
	}
	
	/*
	 * Selects a path from the from the collection of found paths
	 *
	 * Return: true on success, false on failure
	 */
	this.selectPath = function() {
	
		// No paths have been found
		if( this.dstPathLength == Number.POSITIVE_INFINITY )
		{
			return false;
		}
	
		// Start at destination, and move to grids with lower distance values
		var x = this.dst.x;
        var y = this.dst.y;
        for( var i = this.dstPathLength; i >= 0; i-- )
        {
            this.dstPath[i] = { x: x, y: y };
		
            if( x > 0 && this.spaces[x-1][y].distance < this.spaces[x][y].distance )
            {
                x--;
            }
            else if( x < this.spaces.length-1 && this.spaces[x+1][y].distance < this.spaces[x][y].distance )
            {
                x++;
            }
            else if( y > 0 && this.spaces[x][y-1].distance < this.spaces[x][y].distance )
            {
                y--;
            } 
            else if( y < this.spaces[x].length-1 && this.spaces[x][y+1].distance < this.spaces[x][y].distance )
            {
                y++;
            }
        }
				
		return true;
	}
	
	/*
	 * DEBUGGING: Creates a table of the grid spaces array
	 *
	 * Return: HTML code to be added to the DOM
	 */
	this.debug = function() {
		var table = "<table>";
		for( var y = 0; y < this.spaces[0].length; y++ )
		{		
			table += "<tr>";
			
			for( var x = 0; x < this.spaces.length; x++ )
			{
				table += "<td>";
				table += "<span>Sourc Dist.:</span> " + this.spaces[x][y].distance;
				table += "<br />";
				var wall  = ( this.spaces[x][y].wall != null ) ? this.spaces[x][y].wall.typeString : "-";
				table += "<span>Wall:</span> " + wall;
				table += "<br />";
				var food = ( this.spaces[x][y].food ) ? this.spaces[x][y].food.health : "-";
				table += "<span>Food:</span> " + food;
				table += "<br />";
				table += "<span>Pheromones:</span> " + this.spaces[x][y].pheromones;
				table += "</td>";
			}
			
			table += "</tr>";
		}
		table += "</table>";
		
		return table;
	}


	/*
	 * Creates a path back to the grid source
	 *
	 * x - the start grid x position
	 * y - the start grid y position
	 */
	this.createPath = function( x, y ) {
	
		var curDistance = this.spaces[x][y].distance;
		var newPath = [];
		
		// Loop until back at source
		while( x != this.src.x || y != this.src.y )
		{
			// Find adjacent grid with lower distance
			if( x > 0 && this.spaces[x-1][y].distance < curDistance )
			{
				x--;
			}
			else if( x < this.spaces.length-1 && this.spaces[x+1][y].distance < curDistance )
			{
				x++;
			}
			else if( y > 0 && this.spaces[x][y-1].distance < curDistance )
			{
				y--;
			}
			else if( y < this.spaces[x].length-1 && this.spaces[x][y+1].distance < curDistance )
			{
				y++;
			}
			
			curDistance = this.spaces[x][y].distance;
			newPath.push( { x: x, y: y } );
		}
	
		return newPath;
	}
	
	
	/* 
	 * Creates a minified string for recreating a map
	 */
	this.saveMap = function() {
		var map = JSON.stringify( this.spaces );
		map = '{"map":' + map.replace( /\"distance\":null,\"food\":null,\"pheromones\":0,/g, "" ) + ',';
		map += '"src":' + JSON.stringify( this.src ) + ',"dst":'  + JSON.stringify( this.dst ) + '}';
		return map;
	}
	
	
	/*
	 * Sets the specified background at the specified coordinates
	 *
	 * x - x coordinate of click event
	 * y - y coordinate of click event
	 */
	this.setBackground = function( x, y, type, version ) {
		// Round coordinate to grid
		x = Math.floor( x / this.size );
		y = Math.floor( y / this.size );
		
		// Check if source or destination
		if( type == "source" )
		{	
			this.src.x = x;
			this.src.y = y;
			return;
		}
		else if( type == "destination" )
		{
			this.dst.x = x;
			this.dst.y = y;
			return;
		}
		
		if( window.wallTypes[type] != undefined )
		{
			this.spaces[x][y].wall = new Wall( x, y, type, version );
		}
		else
		{
			this.spaces[x][y].wall = null;
		}

		this.spaces[x][y].background = { type: type, version: version };
	}


	/*
	 * Checks if a wall exists at the specified coordinates
	 *
	 * Return: 0 if wall cannot be placed, negative # for cost of placing the wall, positive # for credit for removing the wall
	 *
	 * x - x coordinate of click event
	 * y - y coordinate of click event
	 * type - the wall type
	 */
	this.checkWall = function( x, y, type ) {
		// Round coordinate to grid
		x = Math.floor( x / this.size );
		y = Math.floor( y / this.size );
		
		// Player placed wall
		if( this.spaces[x][y].wall != null && this.spaces[x][y].wall.type.player )
		{
			return this.spaces[x][y].wall.type.cost;
		}
		
		// Empty spot, not source or destination
		if( this.spaces[x][y].wall == null && !compareGridPos( this.src, { x: x, y: y } ) && !compareGridPos( this.dst, { x: x, y: y } ) )
		{
			return -1 * window.wallTypes[type].cost;
		}

		return 0;
	}
	
	
	/*
	 * Toggles the specified wall on or off
	 *
	 * x - x coordinate of click event
	 * y - y coordinate of click event
	 * type - the wall type
	 * version - the sprite version to show
	 */
	this.toggleWall = function( x, y, type, version ) {
		// Round coordinate to grid
		x = Math.floor( x / this.size );
		y = Math.floor( y / this.size );
		this.spaces[x][y].wall = ( this.spaces[x][y].wall != null ) ? null : new Wall( x, y, type, version );
	}
	
	
	/*
	 * Drops a piece of food
	 *
	 * x - x coordinate for food
	 * y - y coordinate for food
	 */
	this.dropFood = function( food, x, y ) {
		// Round coordinate to grid
		x = Math.floor( x / this.size );
		y = Math.floor( y / this.size );
		
		// Check if food already present
		if( this.spaces[x][y].food != null )
		{
			return false;
		}
		
		// Add food
		food.path = this.createPath( x, y );
		food.x = x;
		food.y = y;
		this.spaces[x][y].food = food;
		
		return true;
	}
	
	
	/*
	 * Removes food from space and clears pheromone path
	 *
	 * food - the food to remove
	 */
	this.clearFood = function( food ) {
		var x = food.x;
		var y = food.y;

		// Clear food pheromone at current space
		this.spaces[x][y].pheromones -= food.pheromones;

		// Clear the resot of the pheromones
		for( var i = 0; i < food.path.length; i++ )
		{
			x = food.path[i].x;
			y = food.path[i].y;
			this.spaces[x][y].pheromones -= food.pheromones;
		}
		
		// Delete food object
		this.spaces[food.x][food.y].food = null;
	}
}


function Sprites()
{
	this.loadingPercent = 0;

	this.grassBk = {
		img: new Image(),
		path: "images/grass.png",
		width: 32,
		height: 32,
		version: [
			{
				weight: 7,
				x: 32,
				y: 96
			},
			{
				weight: 1,
				x: 0,
				y: 160
			},
			{
				weight: 1,
				x: 32,
				y: 160
			},
			{
				weight: 1,
				x: 64,
				y: 160
			}
		],
		weightedArray: [],
		
		
		/*
		 * Creates the weighted array for selecting a random type
		 */
		init: function() {
			// For each version, add the specified number to the weighted array
			for( var i = 0; i < this.version.length; i++ )
			{
				for( var j = 0; j < this.version[i].weight; j++ )
				{
					this.weightedArray.push( { type: "grassBk", version: i } );
				}
			}
		},
		
		/*
		 * Selects a wieghted random background to use
		 */
		random: function() {			
			// Select random array element
			var random = Math.floor( Math.random() * ( this.weightedArray.length ) );

			// Return selected type 
			return this.weightedArray[random];
		}
	}
	
	this.waterBk = {
		img: new Image(),
		path: "images/watergrass.png",
		width: 32,
		height: 32,
		version: [
			{ x: 0, y: 0 },
			{ x: 32, y: 0 },
			{ x: 64, y: 0 },
			{ x: 0, y: 32 },
			{ x: 32, y: 32 },
			{ x: 64, y: 32 },
			{ x: 0, y: 64 },
			{ x: 32, y: 64 },
			{ x: 64, y: 64 },
			{ x: 0, y: 96 },
			{ x: 32, y: 96 },
			{ x: 64, y: 96 },
			{ x: 0, y: 128 },
			{ x: 32, y: 128 },
			{ x: 64, y: 128 },
			{ x: 0, y: 160 },
			{ x: 32, y: 160 },
			{ x: 64, y: 160 }
		]
	}
	
	this.source = {
		img: new Image(),
		path: "images/holek.png",
		width: 31,
		height: 32,
		x: 0,
		y: 0
	}
	
	this.destination = {
		img: new Image(),
		path: "images/whitey.png",
		width: 48,
		height: 48,
		x: 0,
		y: 0
	}
	
	this.fence = {
		img: new Image(),
		path: "images/fence_alt.png",
		width: 32,
		height: 32,
		version: [
			{ x: 0, y: 0 },
			{ x: 32, y: 0 },
			{ x: 64, y: 0 },
			{ x: 0, y: 32 },
			{ x: 32, y: 32 },
			{ x: 64, y: 32 },
			{ x: 0, y: 64 },
			{ x: 32, y: 64 },
			{ x: 64, y: 64 },
			{ x: 0, y: 96 },
			{ x: 32, y: 96 },
			{ x: 64, y: 96 },
			{ x: 0, y: 128 },
			{ x: 32, y: 128 },
			{ x: 64, y: 128 }
		]			
	}

	this.slime = {
		img: new Image(),
		path: "images/slime.png",
		width: 32,
		height: 32,
		frames: [0, 32, 64],
		directions: {
			up: 0,
			left: 32,
			down: 64,
			right: 96	
		}
	}
	
	this.veggies = {
		img: new Image(),
		path: "images/Vegies.png",
		width: 32,
		height: 32,
		
		tomatoe: { x: 0, y: 0 },
		strawberry: { x: 32, y: 0 },
		apple: { x: 64, y: 0 },
		carrot: { x: 96, y: 0 },
		pumpkin: { x: 128, y: 0 },
		jack1: { x: 160, y: 0 },
		jack2: { x: 192, y: 0 },
		watermelon1: { x: 224, y: 0 },
		squash: { x: 256, y: 0 },
		cucumber: { x: 288, y: 0 },
		cherries: { x: 320, y: 0 },
		orange: { x: 352, y: 0 },
		pear: { x: 384, y: 0 },
		banana: { x: 416, y: 0 },
		onion: { x: 448, y: 0 },
		brocoli: { x: 480, y: 0 },
		unknown: { x: 0, y: 32 },
		potatoe: { x: 32, y: 32 },
		pepper: { x: 64, y: 32 },
		peas: { x: 96, y: 32 },
		mushroom: { x: 128, y: 32 },
		eggplant: { x: 160, y: 32 },
		radish: { x: 192, y: 32 },
		corn: { x: 224, y: 32 },
		lemon: { x: 256, y: 32 },
		lime: { x:288 , y: 32 },
		bluefruit: { x: 0, y: 64 },
		rotting1: { x: 32, y: 64 },
		rotting2: { x: 64, y: 64 },
		watermelon2: { x: 0, y: 96 }
	}
	
	this.coin = {
		img: new Image(),
		path: "images/coin_gold.png",
		width: 32,
		height: 32,
		frames: [
			{ x: 0, y: 0 },
			{ x: 32, y: 0 },
			{ x: 64, y: 0 },
			{ x: 96, y: 0 },
			{ x: 128, y: 0 },
			{ x: 160, y: 0 },
			{ x: 192, y: 0 },
			{ x: 224, y: 0 }
		]
	}
	
	this.water = {
		img: new Image(),
		path: "images/magic_torrentacle.png",
		width: 128,
		height: 128,
		frames: [
			{ x: 0, y: 0 },
			{ x: 128, y: 0 },
			{ x: 256, y: 0 },
			{ x: 384, y: 0 },
			{ x: 0, y: 128 },
			{ x: 128, y: 128 },
			{ x: 256, y: 128 },
			{ x: 384, y: 128 },
			{ x: 0, y: 256 },
			{ x: 128, y: 256 },
			{ x: 256, y: 256 },
			{ x: 384, y: 256 },
			{ x: 0, y: 384 },
			{ x: 128, y: 384 },
			{ x: 256, y: 384 },
			{ x: 384, y: 384 }
		]
	}
	
	this.ice = {
		img: new Image(),
		path: "images/magic_iceshield_sheet.png",
		width: 128,
		height: 128,
		frames: [
			{ x: 0, y: 0 },
			{ x: 128, y: 0 },
			{ x: 256, y: 0 },
			{ x: 384, y: 0 },
			{ x: 0, y: 128 },
			{ x: 128, y: 128 },
			{ x: 256, y: 128 },
			{ x: 384, y: 128 },
			{ x: 0, y: 256 },
			{ x: 128, y: 256 },
			{ x: 256, y: 256 },
			{ x: 384, y: 256 },
			{ x: 0, y: 384 },
			{ x: 128, y: 384 },
			{ x: 256, y: 384 },
			{ x: 384, y: 384 }
		]
	}
	
	this.fire = {
		img: new Image(),
		path: "images/magic_firelion_big.png",
		width: 128,
		height: 128,
		frames: [
			{ x: 0, y: 0 },
			{ x: 128, y: 0 },
			{ x: 256, y: 0 },
			{ x: 384, y: 0 },
			{ x: 0, y: 128 },
			{ x: 128, y: 128 },
			{ x: 256, y: 128 },
			{ x: 384, y: 128 },
			{ x: 0, y: 256 },
			{ x: 128, y: 256 },
			{ x: 256, y: 256 },
			{ x: 384, y: 256 },
			{ x: 0, y: 384 },
			{ x: 128, y: 384 },
			{ x: 256, y: 384 },
			{ x: 384, y: 384 }
		]
	}
	
	
	/*
	 * Loads the sprite images
	 */
	this.init = function() {
		this.grassBk.init();
		
		var obj = this;
		this.grassBk.img.onload = function() { obj.imageLoaded(); };
		this.waterBk.img.onload = function() { obj.imageLoaded(); };
		this.source.img.onload = function() { obj.imageLoaded(); };
		this.destination.img.onload = function() { obj.imageLoaded(); };
		this.fence.img.onload = function() { obj.imageLoaded(); };
		this.slime.img.onload = function() { obj.imageLoaded(); };
		this.veggies.img.onload = function() { obj.imageLoaded(); };
		this.coin.img.onload = function() { obj.imageLoaded(); };
		this.water.img.onload = function() { obj.imageLoaded(); };
		this.ice.img.onload = function() { obj.imageLoaded(); };
		this.fire.img.onload = function() { obj.imageLoaded(); };
		
		this.grassBk.img.src = this.grassBk.path;
		this.waterBk.img.src = this.waterBk.path;
		this.source.img.src = this.source.path;
		this.destination.img.src = this.destination.path;
		this.fence.img.src = this.fence.path;
		this.slime.img.src = this.slime.path;
		this.veggies.img.src = this.veggies.path;
		this.coin.img.src = this.coin.path;
		this.water.img.src = this.water.path;
		this.ice.img.src = this.ice.path;
		this.fire.img.src = this.fire.path;
				
		this.size = 11;
		this.loadingUnit = Math.ceil( 100 / this.size );
	}
	
	
	/*
	 * Updates the loading message when an image is loaded
	 */
	this.imageLoaded = function() {
		this.loadingPercent += this.loadingUnit;
	}
}


/*
 * Basic slime unit
 *
 * frameLength - the number of frames in the sprite
 * grid - reference to grid object
 */
function Slime( frameLength, grid )
{
	// Stats
	this.health = 100;
	this.attack = 1;
	this.speed = 4; // Increment to use for movement
	this.pheromones = false; // if TRUE, will lay pheromones as it moves creating a path
	this.food = null; // Reference to current food target
	this.hurt = false; // Slime will flash when it is getting hurt
	
	// Display
	this.width = 20;
	this.height = 20;
	this.frameLength = frameLength;
	this.curFrame = 0;
	this.curDirection = "down";
	this.flash = -1; // Toggles the slime on and off for flashing effect
	
	// Grid location
	this.grid = grid;
	this.path = [];
	this.gridPos = { x: grid.src.x, y: grid.src.y };
	this.prevPos = { x: -1, y: -1 };
	this.x = ( grid.src.x * grid.size ) + Math.round( ( grid.size - this.width ) / 2 );
	this.y = ( grid.src.y * grid.size ) + Math.round( ( grid.size - this.height ) / 2 );
	this.gridMovement = { x: 0, y: 0 };
	this.backtracking = false; // True when a deadend has been reached, to avoid endlessly moving betweent the same two spots
		
	// Setup visited grid
	this.visitedGrid = [];
	for( var x = 0; x < grid.spaces.length; x++ )
	{
		this.visitedGrid[x] = [];
		
		for( var y = 0; y < grid.spaces[0].length; y++ )
		{
			this.visitedGrid[x][y] = false;
		}
	}
}


/*
 * Updates slime's x and y position on path
 *
 * Return: true if slime moved, false if it can't move (at end of path)
 */
Slime.prototype.followPath = function() {

	// Get next position
	var newPos = this.path.shift();
	if( !newPos )
	{
		return false;
	}
	
	// Update grid position
	this.gridPos.x = newPos.x;
	this.gridPos.y = newPos.y;

	return true;
}


/*
 * Updates slime's x and y position with wandering pattern
 *
 * Only can move to space that is:
 *    - adjacent
 *    - not blocked
 * Priority for moving (in order from highest to lowest)
 *    - food
 *    - pheromone trail
 *    - unvisited (randomize, if more than one option)
 *    - higher path # (randomize, if more than one option)
 *    - lower path # (backtracking from a dead end) (randomize, if more than one option)	
 *
 * Return: true if slime moved, false if it can't move
 */
Slime.prototype.wander = function() {
	
	var spaces = this.grid.spaces;
	var pheromones = 1;
	var nextPos = { x: -1, y: -1 };
	var newPos = [];
	var visitedPos = [];
	var backtrackPos = [];
	var x = this.gridPos.x;
	var y = this.gridPos.y;
	
	// Currently at food
	if( spaces[x][y].food != null )
	{
		if( spaces[x][y].food.health > 0 )
		{
			return !this.foundFood( spaces[x][y].food );
		}
		else
		{
			this.grid.clearFood( spaces[x][y].food );
		}
	}
		
	// Find all available positions
	if( x < spaces.length-1 && spaces[x+1][y].distance != Number.POSITIVE_INFINITY )
	{
		if( spaces[x+1][y].food != null ) // Food
		{
			nextPos.x = x+1;
			nextPos.y = y;
		}
		else if( spaces[x+1][y].pheromones >= pheromones && spaces[x+1][y].distance > spaces[x][y].distance ) // Pheromones
		{
			nextPos.x = x+1;
			nextPos.y = y;
			pheromones = spaces[x+1][y].pheromones;
		}
		else if( !this.visitedGrid[x+1][y] ) // Unvisited
		{
			newPos.push( {x: x+1, y: y } );
		}
		else if( spaces[x+1][y].distance > spaces[x][y].distance ) // Visited, Towards goal
		{
			visitedPos.push( {x: x+1, y: y } );
		}
		else // Visited, Backtracking
		{
			backtrackPos.push( {x: x+1, y: y } );
		}
	}
	if( y < spaces[x].length-1 && spaces[x][y+1].distance != Number.POSITIVE_INFINITY )
	{
		if( spaces[x][y+1].food != null ) // Food
		{
			nextPos.x = x;
			nextPos.y = y+1;
		}
		else if( spaces[x][y+1].pheromones >= pheromones && spaces[x][y+1].distance > spaces[x][y].distance ) // Pheromones
		{
			nextPos.x = x;
			nextPos.y = y+1;
			pheromones = spaces[x][y+1].pheromones;
		}
		else if( !this.visitedGrid[x][y+1] ) // Unvisited
		{
			newPos.push( {x: x, y: y+1 } );
		}
		else if( spaces[x][y+1].distance > spaces[x][y].distance ) // Visited, Towards goal
		{
			visitedPos.push( {x: x, y: y+1 } );
		}
		else // Visited, Backtracking
		{
			backtrackPos.push( {x: x, y: y+1 } );
		}
	}
	if( x > 0 && spaces[x-1][y].distance != Number.POSITIVE_INFINITY )
	{
		if( spaces[x-1][y].food != null ) // Food
		{
			nextPos.x = x-1;
			nextPos.y = y;
		}
		else if( spaces[x-1][y].pheromones >= pheromones && spaces[x-1][y].distance > spaces[x][y].distance ) // Pheromones
		{
			nextPos.x = x-1;
			nextPos.y = y;
			pheromones = spaces[x-1][y].pheromones;
		}
		else if( !this.visitedGrid[x-1][y] ) // Unvisited
		{
			newPos.push( {x: x-1, y: y } );
		}
		else if( spaces[x-1][y].distance > spaces[x][y].distance ) // Visited, Towards goal
		{
			visitedPos.push( {x: x-1, y: y } );
		}
		else // Visited, Backtracking
		{
			backtrackPos.push( {x: x-1, y: y } );
		}
	}
	if( y > 0 && spaces[x][y-1].distance != Number.POSITIVE_INFINITY )
	{
		if( spaces[x][y-1].food != null ) // Food
		{
			nextPos.x = x;
			nextPos.y = y-1;
		}
		else if( spaces[x][y-1].pheromones >= pheromones && spaces[x][y-1].distance > spaces[x][y].distance ) // Pheromones
		{
			nextPos.x = x;
			nextPos.y = y-1;
			pheromones = spaces[x][y-1].pheromones;
		}
		else if( !this.visitedGrid[x][y-1] ) // Unvisited
		{
			newPos.push( {x: x, y: y-1 } );
		}
		else if( spaces[x][y-1].distance > spaces[x][y].distance ) // Visited, Towards goal
		{
			visitedPos.push( {x: x, y: y-1 } );
		}
		else // Visited, Backtracking
		{
			backtrackPos.push( {x: x, y: y-1 } );
		}			
	}
	
	// Select next grid position. if not already selected
	if( nextPos.x == -1 )
	{
		if( newPos.length > 0 ) // Unvisited
		{
			// Pick random grid
			var random = Math.floor( Math.random() * newPos.length );
			nextPos.x = newPos[random].x;
			nextPos.y = newPos[random].y;
			
			// Set backtracking flag to true
			this.backtracking = false;
		}
		else if( ( !this.backtracking || backtrackPos.length == 0 ) && visitedPos.length > 0 ) // Visited, with higher path #
		{
			// Pick random grid
			var random = Math.floor( Math.random() * visitedPos.length );
			nextPos.x = visitedPos[random].x;
			nextPos.y = visitedPos[random].y;
			
			// Clear backtracking if it is empty
			if( backtrackPos.length == 0 )
			{
				this.backtracking = false;
			}
		}
		else if( backtrackPos.length > 0 ) // Visited, with lower path # (backtracking)
		{
			// Pick random grid
			var random = Math.floor( Math.random() * backtrackPos.length );
			nextPos.x = backtrackPos[random].x;
			nextPos.y = backtrackPos[random].y;
			
			// Set backtracking flag to true
			this.backtracking = true;
		}
		else
		{
			return false;
		}
	}
	
	// Update grid position
	this.gridPos.x = nextPos.x;
	this.gridPos.y = nextPos.y;
	
	// Clear pheromones
	this.pheromones = false;
	
	return true;
}


/*
 * Sets path to food and starts the pheromones
 *
 * food - the food the has been found
 */
Slime.prototype.foundFood = function( food ) {
	
	this.pheromones = true;
	this.setPath( food.path );
	
	// Take some food
	food.eat( this.attack );
	
	return true;
}


/*
 * Sets a new path and updates position on it
 *
 * path - the path to assign
 */
Slime.prototype.setPath = function( path ) {
	if( path == null || path.length == 0 )
	{
		return false;
	}
	
	this.path = [];
	for( var i = 0; i < path.length; i++ )
	{
		this.path[i] = { x: path[i].x, y: path[i].y };
	}
	
	return true;
}


/*
 * Selects the next grid position to move to
 */
Slime.prototype.selectNextPosition = function() {
	
	// Save previous position and update visited grid
	this.prevPos.x = this.gridPos.x;
	this.prevPos.y = this.gridPos.y;
	this.visitedGrid[this.prevPos.x][this.prevPos.y] = true;
	
	// Check for destination
	if( compareGridPos( this.prevPos, this.grid.dst ) )
	{
		return false;
	}
	
	// Lay pheromones
	if( this.pheromones )
	{
		this.grid.spaces[this.gridPos.x][this.gridPos.y].pheromones++;
	}

	// Follow path if it has one, otherwise wander
	var move = true;
	if( this.path.length > 0 )
	{ 
		if( !this.followPath() )
		{
			return false;
		}
	}
	else if( !this.wander() )
	{
		return true;
	}

	// Set movement between grid positions
	this.setGridMovement();
	
	return true;
}


/*
 * Sets movement from previous to current grid position
 */
Slime.prototype.setGridMovement = function() {

	if( this.prevPos.x == this.gridPos.x ) // Move on the y axis
	{
		if( this.prevPos.y < this.gridPos.y ) // Move down
		{
			this.gridMovement.y = this.grid.size;
			this.curDirection = "down";
		}
		else // Move up
		{
			this.gridMovement.y = -this.grid.size;
			this.curDirection = "up";
		}
	}
	else // Move on the x axis
	{
		if( this.prevPos.x < this.gridPos.x ) // Move right
		{
			this.gridMovement.x = this.grid.size;
			this.curDirection = "right";
		}
		else // Move left
		{
			this.gridMovement.x = -this.grid.size;
			this.curDirection = "left";
		}
	}
}


/*
 * Updates slime's x and y position based on speed
 *
 * grid - the current game grid
 * 
 * Returns: true if slime moved, false if slime can't move (at end of path)
 */
Slime.prototype.move = function( grid ) {

	// Update grid position if neccessary
	if( this.gridMovement.x == 0 && this.gridMovement.y == 0 )
	{
		if( !this.selectNextPosition() )
		{
			return false;
		}
	}
	
	// Do x axis movement
	if( this.gridMovement.x > 0 )
	{
		this.x += this.speed;
		this.gridMovement.x -= this.speed;
	}
	else if( this.gridMovement.x < 0 )
	{
		this.x -= this.speed;
		this.gridMovement.x += this.speed;
	}
	
	// Do y axis movement
	if( this.gridMovement.y > 0 )
	{
		this.y += this.speed;
		this.gridMovement.y -= this.speed;
	}
	else if( this.gridMovement.y < 0 )
	{
		this.y -= this.speed;
		this.gridMovement.y += this.speed;
	}
	
	// Update animation frame
	this.curFrame++;
	if( this.curFrame > this.frameLength )
	{
		this.curFrame = 0;
	}
		
	return true;
}


/*
 * Food, used to distract/slow down slimes
 * Has a limited health, and disappears when it reaches zero
 */
function Food( health )
{
	this.health = health;
	this.path = [];
	this.pheromones = 0;
}


/*
 * An slime has taken a some of the food
 *
 * biteSize - the amount to subtract from the food's health
 */
Food.prototype.eat = function( biteSize )
{
	this.pheromones++;
	this.health -= biteSize;
	return ( this.health > 0 ) ? true : false;
}

/*
 * Attack types
 */
function AttackTypes()
{
	this.water = {
		name: "Water Tentacle",
		damage: 5,
		duration: 8,
		delay: 4,
		cost: 20,
		width: 64,
		height: 64
	}

	this.ice = {
		name: "Ice Ball",
		damage: 40,
		duration: 6,
		delay: 8,
		cost: 80,
		width: 64,
		height: 64
	}

	this.fire = {
		name: "Fire Lion",
		damage: 10,
		duration: 6,
		delay: 5,
		cost: 50,
		width: 64,
		height: 64
	}
}

/*
 * The attacks the player can use
 */
function Attacks()
{
	this.types = new AttackTypes();
	this.mana = 100;
	this.maxMana = 100;
	this.curFrame = 0;
	this.frameLength = 0;
	this.curAttack = null;
	this.x = -1;
	this.y = -1;
	
	/*
	 * Starts an attack if enough mana is currently available
	 *
	 * Returns: the amount of damage the attack has done
	 */
	this.use = function( type, frameLength, x, y ) {
		// Don't use if already attacking or not enough mana.
		if( this.curAttack != null || this.mana < this.types[type].cost )
		{
			return false;
		}
		
		// Reduce mana by attack cost
		this.mana -= this.types[type].cost;
		
		// Save current attack frames and centered coordinates
		this.curAttack = type;
		this.frameLength = frameLength-1;
		this.curFrame = 0;
		var widthOffset = Math.round( this.types[type].width / 2 );
		var heightOffset = Math.round( this.types[type].height / 2 );
		this.x = x - widthOffset;
		this.y = y - heightOffset;
		
		return {
			damage: this.types[type].damage,
			duration: this.types[type].duration,
			delay: this.types[type].delay,
			minX: this.x,
			minY: this.y,
			maxX: x + widthOffset,
			maxY: y + heightOffset
		}
	}
	
	
	/*
	 * Updates current attack animations and increases mana
	 */
	this.update = function() {
		// An attack is in progress
		if( this.curAttack )
		{
			// Increase animation frame
			if( this.curFrame < this.frameLength )
			{
				this.curFrame++;
			}
			else // Attack finished
			{
				// Reset stats
				this.curAttack = null;
				this.frameLength = 0;
				this.curFrame = 0;
				this.x = -1;
				this.y = -1;
			}
		}
		
		// Increase mana
		if( this.mana < this.maxMana )
		{
			this.mana++;
		}
	}
	
	
	/*
	 * Returns true if specified attack can be used, false if not
	 */
	this.isReady = function( type ) {
		if( this.types[type] )
		{
			return ( this.types[type].cost <= this.mana );
		}
		
		return false;
	}
	
	
	/*
	 * Resets any current attack
	 */
	this.reset = function() {
		this.mana = 100;
		this.curFrame = 0;
		this.frameLength = 0;
		this.curAttack = null;
		this.x = -1;
		this.y = -1;
	}

}


/*
 * Coins which are dropped by dead enemies and collected by player
 */
function Coin( x, y, frameLength )
{
	this.x = x;
	this.y = y;
	this.width = 32;
	this.height = 32;
	this.value = 1;
	this.life = 48;
	this.frameLength = frameLength;
	this.curFrame = 0;
}


/*
 * Decrements a coins life and updates it's animation
 * Returns: false if coin has expired, true if not
 */
Coin.prototype.update = function()
{
	// Update frame
	if( this.curFrame < this.frameLength )
	{
		this.curFrame++;
	}
	else
	{
		this.curFrame = 0;
	}
	
	// Update life
	this.life--;
	return ( this.life > 0 );
}

window.wallTypes = {
	fence: {
		player: true,
		cost: 1
	},
	waterBk: {
		player: false
	}
}

function Wall( x, y, type, version )
{
	this.type = window.wallTypes[type];
	this.typeString = type;
	this.version = version;
	this.x = x;
	this.y = y;	
}


/*
 * Compares two grid position
 *
 * Returns: true for equal, false if not
 */
function compareGridPos( a, b )
{
	if( a.x == b.x && a.y == b.y )
	{
		return true;
	}
	
	return false;
}