/*
 * Slime Attack - Liberated Pixel Cup Contest Entry
 * Copyright (C) 2012 Andrew Giles
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var game = {
	debug: true,
	
	// Level info
	createMode: false,
	buildPhase: true,
	level: null,
	round: 0,
	
	// Animation
	loaded: false,
	framerate: 50,
	timeout: null,
	stopping: false,
	zoom: 1,
	
	// Canvas
	displayGrid: false,
	displayPath: false,
	displaySrcDst: true,
	
	// Sprites
	sprites: new Sprites(),
	
	// Walls/Bks
	selectedWall: "fence_0",
	selectedBk: "destination",
	
	// Coins
	coinChance: 0.2,
	coins: [],
	defaultPlayerCoins: 10,
	
	// Slimes
	defaultSlimeCount: 30,
	defaultSlimeRate: 10,	
	slimeTimer: 0,
	slimes: [],
	
	// Attacks
	selectedAttack: "water",
	curAttack: false,
	attackDelay: 0,
	attackDuration: 0,
	attacks: new Attacks(),
		
	
	/*
	 * Loads images and starts loading message
	 */
	init: function() {
		this.bk_canvas = document.getElementById( "staticCanvas" );
		this.bk_context = this.bk_canvas.getContext( "2d" );
		this.an_canvas = document.getElementById( "animatedCanvas" );
		this.an_context = this.an_canvas.getContext( "2d" );
		
		this.grid = new Grid( this.bk_canvas.width, this.bk_canvas.height, this.sprites.grassBk );
		
		this.live_slime_count = this.defaultSlimeCount;
		this.playerCoins = this.defaultPlayerCoins;
			
		this.sprites.init();
		this.initMenus();
		this.load();		
	},
	
	
	/*
	 * Set appropriate styling for wall/terrain menu items
	 */
	initMenus: function() {
		var html = '';
		for( var w in this.walls ) {
			var wall = this.walls[w];
			html += '<div class="wall">';
			html += '<a href="'+wall.id+'" class="'+wall.id+'" style="background-positiion: px px;"></a>';
			html += '</div>';
		}
		$('#wallMenu').html( html );
	
		/*$('#wallMenu').find( 'a' ).each( function( i, element ) {
			var wall = game.getSelectedWall( $(element).attr( 'href' ) );
			$(element).addClass( wall.type );
			
			var x, y;
			if( wall.version != undefined )
			{
				x = game.sprites[wall.type].version[wall.version].x;
				y = game.sprites[wall.type].version[wall.version].y;
			}
			else
			{
				x = game.sprites[wall.type].x;
				y = game.sprites[wall.type].y;
			}
			
			$(element).css( "background-position", "-"+x+"px -"+y+"px" );
		} );*/
		
		$('#required').find( 'a' ).each( function( i, element ) {
			var type = $(element).attr( 'href' );
			$(element).addClass( type );
		} );
		
		$('#grass').find( 'a' ).each( function( i, element ) {
			var wall = game.getSelectedWall( $(element).attr( 'href' ) );
			$(element).addClass( wall.type );

			var x, y;
			if( wall.version != undefined )
			{
				x = game.sprites[wall.type].version[wall.version].x;
				y = game.sprites[wall.type].version[wall.version].y;
			}
			else
			{
				x = game.sprites[wall.type].x;
				y = game.sprites[wall.type].y;
			}
			
			$(element).css( "background-position", "-"+x+"px -"+y+"px" );
		} );
		
		$('#water').find( 'a' ).each( function( i, element ) {
			var wall = game.getSelectedWall( $(element).attr( 'href' ) );
			$(element).addClass( wall.type );
			
			var x, y;
			if( wall.version != undefined )
			{
				x = game.sprites[wall.type].version[wall.version].x;
				y = game.sprites[wall.type].version[wall.version].y;
			}
			else
			{
				x = game.sprites[wall.type].x;
				y = game.sprites[wall.type].y;
			}
			
			$(element).css( "background-position", "-"+x+"px -"+y+"px" );
		} );
	},
	
	
	/*
	 * Displays loading message while sprites load
	 */
	load: function() {
		
		// Finished loading
		if( game.sprites.loadingPercent >= 100 ) {
			$('#mainMenu p').hide();
			$('#menuLinks').show();
			game.loaded = true;
			
			if( game.debug ) {
				$('#play').show();
				game.loadLevel( window.levels['level_1'] );			
			}
			return;
		}
		
		// Show loading message
		$('#mainMenu p').html( "Loading..."+game.sprites.loadingPercent+"%" );
		
		// Loop
		setTimeout( game.load, game.framerate );		
	},

	
	/*
	 * Prepares map
	 */
	setup: function( level ) {
		this.level = level;
	
		// Initialize grid
		if( this.level )
		{
			this.grid.init( level.src, level.dst, level.map );
			this.grid.initPaths();
		}
		else
		{
			this.grid.init();
		}
		
		// Initial draw
		this.an_context.clearRect( 0, 0, this.an_canvas.width, this.an_canvas.height );
		this.drawStatic();
		this.drawGridObjects();
	},
		
	
	/*
	 * Calculates path, draws everything, starts the slimes
	 */
	start: function() {
		// Already started
		if( this.timeout != null )
		{
			return;
		}
		
		// Ensure source and destination exist
		if( this.grid.src.x == -1 || this.grid.dst.x == -1 )
		{
			this.showMessage( "Princess and Hole are required." );
			return;
		}
		
		// Reset, create, and print path
		this.grid.resetPath();
		var validPath = this.grid.initPaths();

		// Draw static elements
		this.drawStatic();
		
		// If no path is found, alert player and don't start
		if( !validPath )
		{
			this.showMessage( "There must be one path to the Princess." );
			return;
		}
		
		if( this.createMode )
		{
			// Save current level
			this.level = JSON.parse( this.grid.saveMap() );
			
			// Set rounds if not set already
			if( !this.level.rounds )
			{
				this.level.rounds = [ { count: this.defaultSlimeCount, rate: this.defaultSlimeRate } ];
			}
		
			// Hide create menu, show player stats and wall menu
			$('#createMenu').hide();
			$('#createControls').hide();
			$('#playerStats').show();
			$('#wallMenu').show();
			
			this.createMode = false;
			this.buildPhase = true;
		}
		else
		{
			// Hide wall menu, show item/attack menu
			$('#wallMenu').hide();
			$('#itemMenu').show();
			$('#attackMenu').show();
			
			// Disable start button
			$('#controlMenu').find( 'a[href="start"]' ).addClass( 'disabled' );
			
			this.buildPhase = false;
			
			// Start loop
			this.timeout = setTimeout( this.loop, this.framerate );
		}
	},
	
	
	/*
	 * Stops loop and resets all game stats
	 */
	restart: function() {
		// Stop loop
		clearTimeout( this.timeout );
		this.timeout = null;
		this.buildPhase = true;
					
		// Reset coins
		this.coins = [];
		this.playerCoins = this.defaultPlayerCoins;
		
		// Reset slimes
		if( this.level )
		{
			this.live_slime_count = this.level.rounds[this.round].count;
		}
		else
		{
			this.live_slime_count = this.defaultSlimeCount;
		}
		this.slimeTimer = 0;
		this.slimes = [];
		
		// Reset attacks
		this.curAttack = false;
		this.attackDelay = 0;
		this.attackDuration = 0;
		this.attacks.reset();
		
		// Reset grid
		this.setup( this.level );
		
		// Update mana/coins
		$('#mana').find('span').width( this.attacks.mana+"px" );
		$('#coins').html( this.playerCoins );
		
		// Show wall menu, hide item/attack menu
		if( !this.createMode )
		{
			$('#itemMenu').hide();
			$('#attackMenu').hide();
			$('#wallMenu').show();
		}
				
		// Enable start button
		$('#controlMenu').find( 'a[href="start"]' ).removeClass( 'disabled' );
	},
		
	
	/*
	 * Pauses/unpauses the game
	 */
	pause: function() {
		if( this.timeout == null && !this.buildPhase )
		{
			// Start loop
			this.timeout = setTimeout( this.loop, this.framerate );
		}
		else
		{
			// Stop loop
			clearTimeout( this.timeout );
			this.timeout = null;
		}
	},
	
	
	nextRound: function() {
		// Stop loop
		clearTimeout( this.timeout );
		this.timeout = null;
		this.stopping = false;
		this.buildPhase = true;
		
		// Increase round
		this.round++;

		// Reset coins
		this.coins = [];
		
		// Reset slimes
		this.live_slime_count = this.level.rounds[this.round].count;
		this.slimeTimer = 0;
		this.slimes = [];
		
		// Reset attacks
		this.curAttack = false;
		this.attackDelay = 0;
		this.attackDuration = 0;
		this.attacks.reset();
		$('#mana').find('span').width( this.attacks.mana+"px" );
		
		// Reset grid
		this.grid.reset();
		
		// Clear animated canvas
		this.an_context.clearRect( 0, 0, this.an_canvas.width, this.an_canvas.height );
				
		// Show wall menu, hide item/attack menu
		if( !this.createMode )
		{
			$('#itemMenu').hide();
			$('#attackMenu').hide();
			$('#wallMenu').show();
		}
				
		// Enable start button
		$('#controlMenu').find( 'a[href="start"]' ).removeClass( 'disabled' );
		
		// Show round title
		$('#level').html( "Round "+(this.round+1) );
		$('#level').show();
		$('#level').delay( 2000 ).fadeOut();
	},
	
	
	/*
	 * Stops loop and shows win/lose screen
	 */
	endGame: function( win ) {
		// Reset game
		this.restart();
		this.stopping = false;
	
		// Add message
		if( win )
		{
			$('#gameover').find( 'p' ).html( "Congrats, you saved the princess!" );
		}
		else
		{
			$('#gameover').find( 'p' ).html( "Oh no, the princess was killed! Try again." );
		}
	
		$('#gameover').show();
		$('#mainMenu').show();
	},
	
	
	/*
	 * Loops game cycles
	 */
	loop: function() {
		// Static canvas needs updating
		if( game.grid.refreshStatic )
		{
			game.drawStatic();
			game.grid.refreshStatic = false;
		}
		
		// Updated animated canvas
		game.an_context.clearRect( 0, 0, game.an_canvas.width, game.an_canvas.height );
		game.drawGridObjects();
		game.updateSlimes();
		game.drawAttacks();	
		
		// Update the attacks
		game.updateAttacks();
		
		// Update coins
		game.updateCoins();
		
		// Update mana in hud
		$('#mana').find('span').width( game.attacks.mana+"px" );
		
		// DEBUGGING TABLE
		if( game.debug )
		{
			$('#debug').html( game.grid.debug() );
		}
		
		// Check if all slimes are dead
		if( game.slimes.length == 0 && game.live_slime_count <= 0 && !game.stopping )
		{
			var callback;
			if( game.round < game.level.rounds.length-1 )
			{
				// Start next round
				callback = function() { game.nextRound(); }
			}
			else
			{
				// Finished level, show win screen
				callback = function() { game.endGame( true ); }
			}
			
			// Delay so animations have time to finish
			game.stopping = true;
			setTimeout( callback, 2000 );
		}
		
		game.timeout = setTimeout( game.loop, game.framerate );
	},

	
	/*
	 * Draws static elements
	 */
	drawStatic: function() {
		// Clear canvas
		this.bk_context.clearRect( 0, 0, this.bk_canvas.width, this.bk_canvas.height );
		
		// Draw all static elements
		this.drawGridObjects( true );
		if( this.displayGrid )
		{
			this.drawGridLines();
		}
		if( this.displaySrcDst )
		{
			this.drawStartEnd();
		}
		if( this.displayPath )
		{
			this.drawPath();
		}
	},
	
	
	/*
	 * Draws animated elements
	 */
	drawAnimated: function() {
		// Clear animated canvas
		this.an_context.clearRect( 0, 0, this.an_canvas.width, this.an_canvas.height );
		
		// Redraw objects
		this.drawGridObjects();
		this.drawAttacks();		
	},
	
	
	/*
	 * Draws grid on the background canvas
	 */
	drawGridLines: function() {
	
		this.bk_context.strokeStyle = "#ffffff";
		this.bk_context.fillStyle = "#ffffff";
	
		this.bk_context.beginPath();
    
		// Loop through x positions
		for( var x = 0; x <= this.bk_canvas.width; x += this.grid.size )
		{
			this.bk_context.moveTo( x, 0 );
			this.bk_context.lineTo( x, this.bk_canvas.height );
		}
		
		// Loop through y positions
		for( var y = 0; y <= this.bk_canvas.height; y += this.grid.size )
		{
			this.bk_context.moveTo( 0, y );
			this.bk_context.lineTo( this.bk_canvas.width, y );
		}
		
		this.bk_context.closePath();
		this.bk_context.stroke();
	},
	
	
	/*
	 * Draws start and end positions
	 */
	drawStartEnd: function() {
		this.bk_context.drawImage( this.sprites.source.img, this.sprites.source.x, this.sprites.source.y, this.sprites.source.width, this.sprites.source.height, this.grid.src.x * this.grid.size, this.grid.src.y * this.grid.size, this.grid.size, this.grid.size );
		this.bk_context.drawImage( this.sprites.destination.img, this.sprites.destination.x, this.sprites.destination.y, this.sprites.destination.width, this.sprites.destination.height, this.grid.dst.x * this.grid.size, this.grid.dst.y * this.grid.size, this.grid.size, this.grid.size );
	},
	
	
	/*
	 * Draws path
	 */
	drawPath: function() {
		this.bk_context.strokeStyle = "#F4A83D";
		this.bk_context.fillStyle = "#F4A83D";
		var pathSize = this.grid.size / 2;
		var pathOffset = Math.round( pathSize / 2 );
		for( var i = 1; i < this.grid.dstPath.length-1; i++ )
		{
			var pos = this.grid.dstPath[i];
			this.bk_context.fillRect( ( pos.x * this.grid.size ) + pathOffset, ( pos.y * this.grid.size ) + pathOffset, pathSize, pathSize );
		}
	},
	
	
	/*
	 * Draws the content of each grid square
	 */
	drawGridObjects: function( refreshStatic ) {
	
		for( var x = 0; x < this.grid.spaces.length; x++ )
		{
			for( var y = 0; y < this.grid.spaces[x].length; y++ )
			{
				if( refreshStatic )
				{
					// Draw background
					var bkType = this.sprites[this.grid.spaces[x][y].background.type];
					var bkVersion = bkType.version[this.grid.spaces[x][y].background.version];
					this.bk_context.drawImage( bkType.img, bkVersion.x, bkVersion.y, bkType.width, bkType.height, ( x * this.grid.size ), ( y * this.grid.size ), this.grid.size, this.grid.size );
				
					// Draw walls
					if( this.grid.spaces[x][y].wall && this.grid.spaces[x][y].wall.type.player )
					{
						var imgX, imgY;
						var sprite = this.sprites[this.grid.spaces[x][y].wall.typeString];

						if( this.grid.spaces[x][y].wall.version != undefined )
						{
							imgX = sprite.version[this.grid.spaces[x][y].wall.version].x;
							imgY = sprite.version[this.grid.spaces[x][y].wall.version].y;
						}
						else
						{
							imgX = sprite.x;
							imgY = sprite.y;
						}

						this.bk_context.drawImage( sprite.img, imgX, imgY, sprite.width, sprite.height, ( x * this.grid.size ), ( y * this.grid.size ), this.grid.size, this.grid.size );
					}
				}
				
				// Draw pheromones
				var pheromones = this.grid.spaces[x][y].pheromones;
				if( pheromones > 0 )
				{
					var radius = Math.round( pheromones / 2 );
					var offset = Math.round( this.grid.size / 2 );
					var xPos = ( x * this.grid.size ) + offset;
					var yPos = ( y * this.grid.size ) + offset;
					
					this.an_context.strokeStyle = "#5acd73";
					this.an_context.fillStyle = "#5acd73";
					this.an_context.beginPath();
					this.an_context.arc( xPos, yPos, radius, 0, Math.PI*2, true );
					this.an_context.closePath();
					this.an_context.fill();
				}
				
				// Draw food
				if( this.grid.spaces[x][y].food != null )
				{
					this.an_context.drawImage( this.sprites.veggies.img, this.sprites.veggies.strawberry.x, this.sprites.veggies.strawberry.y, this.sprites.veggies.width, this.sprites.veggies.height, ( x * this.grid.size )+3, ( y * this.grid.size )+5, this.grid.size-10, this.grid.size-10 );
				}
			}
		}
	},
	
	
	/*
	 * Draws slime on canvas
	 */
	drawSlime: function( slime ) {		
		// Draw slime
		if( slime.flash < 0 )
		{
			this.an_context.drawImage( this.sprites.slime.img, this.sprites.slime.frames[slime.curFrame], this.sprites.slime.directions[slime.curDirection], this.sprites.slime.width, this.sprites.slime.height, slime.x, slime.y, slime.width, slime.height );
		}
	},
	
	
	/*
	 * Creates slimes with a timed delay
	 */
	createSlimes: function() {
		// No more slimes to create
		if( this.live_slime_count <= 0 )
		{
			return;
		}
		
		// Only create a new slime if appropriate time has passed
		if( this.slimeTimer != this.level.rounds[this.round].rate )
		{
			this.slimeTimer++;
			return;
		}
		
		// Reset timer
		this.slimeTimer = 0;
			
		// Create new slime and increment count;
		var slime = new Slime( this.sprites.slime.frames.length-1, this.grid );
		this.slimes.push( slime );
		this.live_slime_count--;	
	},
	
	
	/*
	 * Creates new slimes, moves slimes, deletes dead/finished slimes, and draws slimes.
	 */
	updateSlimes: function() {
		// Create new slimes
		this.createSlimes();	
	
		var length = this.slimes.length-1;
		
		for( var i = length; i >= 0; i-- )
		{	
			var slime = this.slimes[i];
			
			// Move slime
			if( !slime.move() )
			{
				// If at destination, end game
				// Delay ending so animations have time to finish
				if( !this.stopping )
				{
					this.stopping = true;
					setTimeout( function() { game.endGame( false ); }, 1000 );
				}
				return;
			}
			
			// Update flash
			if( slime.hurt )
			{
				slime.flash *= -1;
			}
			else
			{
				slime.flash = -1;
			}
			
			// Draw slime
			this.drawSlime( slime );
			
			// Reset hurt
			slime.hurt = false;
		}
	},
	
	
	/*
	 * Draw the current attack animation
	 */
	drawAttacks: function() {
		if( this.attacks.curAttack )
		{
			var type = this.attacks.curAttack;
			var img = this.sprites[type].img;
			var imgCoords = this.sprites[type].frames[this.attacks.curFrame];
			var imgWidth = this.sprites[type].width;
			var imgHeight = this.sprites[type].height;
			this.an_context.drawImage( img, imgCoords.x, imgCoords.y, imgWidth, imgHeight, this.attacks.x, this.attacks.y, this.attacks.types[type].width, this.attacks.types[type].height );
		}
	},
	
	
	/*
	 * Start an attack at the specified coordinates
	 */
	attack: function( x, y ) {
		// Start attack
		this.curAttack = this.attacks.use( this.selectedAttack, this.sprites[this.selectedAttack].frames.length, x, y );
		
		// Save attack duration
		this.attackDuration = this.curAttack.duration;
		
		// Delay damage to match animation
		this.attackDelay = this.curAttack.delay;
	},
	
	
	/*
	 * Updates any running attacks
	 */
	updateAttacks: function() {
		// Update attack data
		this.attacks.update();
		
		// Update attack menu
		$('#attackMenu').find( 'a' ).each( function( i, element ) {
			var type = $(this).attr( 'href' );
			if( game.attacks.isReady( type ) )
			{
				$('#attackMenu').find( 'a[href="'+type+'"]' ).removeClass( 'disabled' );
			}
			else
			{
				$('#attackMenu').find( 'a[href="'+type+'"]' ).addClass( 'disabled' );
			}
		} );
			
		if( this.curAttack )
		{
			// Check if attack is finished it's delay
			if( this.attackDelay == 0 )
			{
				// Attempt to damage slimes
				var length = this.slimes.length-1;
				for( var i = length; i >= 0; i-- )
				{
					var slime = this.slimes[i];
					
					// Check if hit by attack
					if( slime.x < this.curAttack.maxX && ( slime.x + slime.width ) > this.curAttack.minX && slime.y < this.curAttack.maxY && ( slime.y + slime.width ) > this.curAttack.minY )
					{
						// Damage slime
						slime.health -= this.curAttack.damage;
						slime.hurt = true;
						
						// Check if dead
						if( slime.health <= 0 )
						{						
							// Attempt to drop a coin
							if( Math.random() < this.coinChance )
							{
								this.coins.push( new Coin( slime.x, slime.y, this.sprites.coin.frames.length-1 ) );
							}
							
							// Remove slime from array and delete it
							this.slimes.splice( i, 1 );
							delete slime;
						}					
					}
				}
				
				if( this.attackDuration > 0 )
				{
					// Decrease attack duration
					this.attackDuration--;
				}
				else
				{
					// Clear attack
					this.curAttack = false;
					this.attackDelay = 0;
				}
				
			}
			else
			{
				this.attackDelay--;
			}
		}
	},


	/*
	 * Sets the background in create mode
	 */
	setBackground: function( x, y ) {
		var wall = this.getSelectedWall( this.selectedBk );
		this.grid.setBackground( x, y, wall.type, wall.version );
	},
	
	
	/*
	 * Toggles a wall on and off
	 */
	toggleWall: function( x, y ) {
		var wall = this.getSelectedWall( this.selectedWall );
		var cost = this.grid.checkWall( x, y, wall.type );
		if( cost == 0 || this.playerCoins + cost < 0 )
		{
			return;
		}

		this.updatePlayerCoins( cost );
		
		var wall = this.getSelectedWall( this.selectedWall );
		this.grid.toggleWall( x, y, wall.type, wall.version );
	},

	
	/*
	 * Drops a bait
	 */
	useBait: function( x, y ) {
		if( this.playerCoins > 0 )
		{
			// Drop food
			var success = this.grid.dropFood( new Food( 20 ), x, y );
			
			if( success )
			{
				// Update player coins
				this.updatePlayerCoins( -1 );
			}
		}
	},
	
	
	/*
	 * Updates the player's coin count
	 */
	updatePlayerCoins: function( change ) {
		// Update coins
		this.playerCoins += change;
		$('#coins').html( this.playerCoins );
				
		if( this.playerCoins > 0 )
		{
			// Renable bait button
			$('#itemMenu').find( 'a[href="food"]' ).removeClass( 'disabled' );
		}
		else
		{
			// Disable bait button
			$('#itemMenu').find( 'a[href="food"]' ).addClass( 'disabled' );
		}
	},
	
	
	/*
	 * Updates any active coins
	 */
	updateCoins: function() {
		var sprite = this.sprites.coin;	
		var length = this.coins.length-1;
		for( var i = length; i >= 0; i-- )
		{
			// Update coin
			var coin = this.coins[i];
			var alive = coin.update();
			
			if( alive )
			{
				// Draw coin
				this.an_context.drawImage( sprite.img, sprite.frames[coin.curFrame].x, sprite.frames[coin.curFrame].y, sprite.width, sprite.height, coin.x, coin.y, coin.width, coin.height );
			}
			else
			{
				// Delete coin
				this.coins.splice( i, 1 );
				delete coin;
			}
		}
	},
	
	
	/*
	 * Adds coin to player's stash
	 */
	grabCoin: function( x, y ) {
		var length = this.coins.length-1;
		for( var i = length; i >= 0; i-- )
		{
			var coin = this.coins[i];
			
			if( x >= coin.x && x <= (coin.x + coin.width ) && y >= coin.y && y <= (coin.y + coin.height ) )
			{
				// Increment coins
				this.updatePlayerCoins( 1 );

				// Delete coin
				this.coins.splice( i, 1 );
				delete coin;
				
				return true;
			}
		}
		
		return false;
	},
	
	
	/*
	 * Returns the type and version of the selected wall
	 */
	getSelectedWall: function( wall ) {	
		var type, version;
		var versionIndex = wall.indexOf( "_" );
		if( versionIndex == -1 )
		{
			
			type = wall;
		}
		else
		{
			type = wall.substring( 0, versionIndex );
			version = wall.substr( versionIndex+1 );
		}
		
		return { type: type, version: version };
	},
	
	
	/*
	 * Shows a message overlay to player for 8s and then fades out
	 */
	showMessage: function( msg ) {
		$('#alert').html( msg );
		$('#alert').show();
		$('#alert').delay( 8000 ).fadeOut();
	},
	
	
	/*
	 * Loads the specified leevl
	 */
	loadLevel: function( level ) {
		// Parse level if required
		if( typeof level == "string" )
		{
			level = JSON.parse( level );
		}
		
		// Set rounds if not set already
		if( !level.rounds )
		{
			level.rounds = [ { count: game.defaultSlimeCount, rate: game.defaultSlimeRate } ];
		}
		
		game.createMode = false;
		game.setup( level );
		game.live_slime_count = game.level.rounds[game.round].count;
		
		// Hide and show appropriate menus
		$('#createMenu').hide();
		$('#createControls').hide();
		$('#playerStats').show();
		$('#wallMenu').show();
		$('#mainMenu').hide();
		$('#play').hide();
		$('#load').hide();
		
		// Show round title
		$('#level').html( "Round "+(this.round+1) );
		$('#level').show();
		$('#level').delay( 2000 ).fadeOut();
	},
	
	
	/*
	 * Populates and opens the save map textarea.
	 */
	saveMap: function() {
		var map = this.grid.saveMap();
		$('#save').find( 'textarea.loadArea' ).val( map );
		$('#save').show();
	},
	
	
	/*
	 * Open the load level textarea
	 */
	loadMap: function( map ) {
		$('#load').find( 'textarea.loadArea' ).val( "" );
		$('#load').show();
	}
}


// Initial setup
$(document).ready( function() {
	// Init game, with callback function for loading event
	game.init();
	
	// Setup menu buttons
	$('#menuLinks').on( 'click', 'a', function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		if( !game.loaded )
		{
			return;
		}
		
		var action = $(this).attr( 'href' );
		
		if( action == "play" ) {
			$('#play').show();
		}
		else if( action == "options" )
		{
			$('#options').show();
		}
		else if( action == "help" )
		{
			$('#help').show();
		}
	} );
	
	// Setup audio slider
	$('#audioSlider').slider( {
		value: 100,
		slide: function( event, ui ) {
			document.getElementById('menuMusic').volume = ( ui.value / 100 );
		}
	} );
	if( game.debug ) {
		document.getElementById('menuMusic').pause();
		document.getElementById('menuMusic').src = '';
	}
	
	// Setup canvas click event
	$('#animatedCanvas').click( function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		if( !game.loaded )
		{
			return;
		}
		
		// Get click coordinates
		var x = event.pageX - $('#animatedCanvas').offset().left;
		var y = event.pageY - $('#animatedCanvas').offset().top;
		
		// Slimes have not started moving
		if( game.timeout == null )
		{
			if( game.createMode )
			{
				// Toggle background
				game.setBackground( x, y );
			}
			else
			{
				// Toggle wall
				game.toggleWall( x, y );
			}
			
			game.drawStatic();
		}
		else // Slimes are moving
		{
			// Try and grad a coin
			if( game.grabCoin( x, y ) )
			{
				return;
			}
			else if( game.selectedAttack == "food" )
			{
				// Use bait
				game.useBait( x, y );
			}
			else
			{
				// Attack
				game.attack( x, y );
			}			
		}
	} );
	
	// Setup play menu
	$('#play').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		var level = $(this).attr( 'href' );
		
		if( level == "create" )
		{
			game.createMode = true;
			game.setup();
			$('#createMenu').show();
			$('#createControls').show();
			$('#playerStats').hide();
			$('#wallMenu').hide();
			$('#attackMenu').hide();
			$('#itemMenu').hide();
			$('#mainMenu').hide();
			$('#play').hide();
		}
		else if( level == "back" )
		{
			$('#play').hide();
		}		
		else
		{
			game.loadLevel( window.levels[level] );
		}
	} );
	
	// Setup pause menu
	$('#pause').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		var action = $(this).attr( 'href' );
		
		if( action == "return" )
		{
			$('#pause').hide();
			game.pause();
		}
		else if( action == "help" )
		{
			$('#help').show();
		}
		else if( action == "options" )
		{
			$('#options').show();
		}
		else if( action == "quit" )
		{
			game.restart();
			$('#mainMenu').show();
			$('#pause').hide();
		}
	} );
	
	// Setup create menu
	$('#createMenu').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		// Set active background
		game.selectedBk = $(this).attr( 'href' );
		
		// Updated menu
		$('#createMenu').find( 'a' ).removeClass( 'active' );
		$(this).addClass( 'active' );
		
	} );
	$('#createMenu').pajinate( {
		items_per_page: 2,
		num_page_links_to_display: 3,
		nav_label_prev: "<-",
		nav_label_next: "->",
		wrap_around: true,
		show_first_last: false
	} );
	
	// Setup wall menu
	$('#wallMenu').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		// Set active active
		game.selectedWall = $(this).attr( 'href' );
		
		// Updated menu
		$('#wallMenu').find( 'a' ).removeClass( 'active' );
		$(this).addClass( 'active' );
		
	} );
	
	// Setup item/attack menu
	$('#attackMenu, #itemMenu').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		// Set active active
		game.selectedAttack = $(this).attr( 'href' );
		
		// Updated menu
		$('#attackMenu').find( 'a' ).removeClass( 'active' );
		$('#itemMenu').find( 'a' ).removeClass( 'active' );
		$(this).addClass( 'active' );		
	} );
	
	// Set attack menu keyboard shortcuts
	$(document).keydown( function( event ) {
		var code = ( event.keyCode ? event.keyCode : event.which );
		var index = -1;
		if( code == 49 || code == 97 ) // 1
		{
			index = 0;
		}
		else if( code == 50 || code == 98 ) // 2
		{
			index = 1;
		}
		else if( code == 51 || code == 99 ) // 3
		{
			index = 2;
		}
		
		if( index != -1 )
		{
			game.selectedAttack = $('#attackMenu').find( 'a:eq('+index+')' ).attr( 'href' );
			
			// Update menu
			$('#attackMenu').find( 'a' ).removeClass( 'active' );
			$('#itemMenu').find( 'a' ).removeClass( 'active' );
			$('#attackMenu').find( 'a:eq('+index+')' ).addClass( 'active' )
		}

	} );
	
	// Setup controls menu
	$('#controlMenu').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		if( !game.loaded )
		{
			return;
		}

		var action = $(this).attr( 'href' );
		if( action == "start" )
		{
			game.start();
		}
		else if( action == "restart" )
		{
			game.restart();
		}
		else if( action == "menu" )
		{
			game.pause();
			$('#pause').show();
		}
		else if( action == "save" )
		{
			game.saveMap();
		}
		else if( action == "load" )
		{
			game.loadMap();
		}
	} );
	
	
	// Setup back buttons
	$('#gameover, #options, #help, #save').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		var action = $(this).attr( 'href' );
		if( action == "menu" )
		{
			$('#gameover').hide();
			$('#options').hide();
			$('#help').hide();
			$('#save').hide();
		}
	} );
	
	
	// Setup load menu
	$('#load').on( "click", "a", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		var action = $(this).attr( 'href' );
		if( action == "load" )
		{
			game.loadLevel( $('#load').find( 'textarea.loadArea' ).val() );
		}
		else
		{
			$('#load').hide();
		}
	} );
	
	
	// Setup help menu
	$('#helpPages').pajinate( {
		nav_panel_id: ".page_navigation_alt", 
		items_per_page: 1,
		num_page_links_to_display: 10,
		nav_label_prev: "<-",
		nav_label_next: "->",
		wrap_around: false,
		show_first_last: false
	} );
		
		
	// Setup options checkboxs
	$('#optionsForm').on( "change", "input", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		if( !game.loaded )
		{
			return;
		}
		
		// Set the appropriate option on or off
		game[$(this).val()] = $(this).is( ":checked" );
		game.drawStatic();
	} );
		
	// Setup debug option checkboxs
	$('#debugOptionsForm').on( "change", "input", function( event ) {
		event.stopPropagation();
		event.preventDefault();
		
		if( !game.loaded )
		{
			return;
		}
		
		// Set the appropriate option on or off
		game[$(this).val()] = $(this).is( ":checked" );
		game.drawStatic();
	} );
	
} );