Code authors
============

This program was written by the following people:
 - Andrew Giles


Asset attribution
=================

Assets were authored by the following people:

Brandon
 - Calmtown.ogg (mp3 version created from orginal by Andrew Giles)
 
Cody Boisclair
 - pressstart2p-webfont.ttf (eot, svg, woff versions created from original by Andrew Giles)
 
Lanea Zimmerman
 - grass.png
 - holek.png
 - watergrass.png

Charles Sanchez
 - slime.png
 
Clint Bellanger
 - coin_gold.png

Daniel Eddeland
 - fence_alt.png
 - magic_firelion_big.png
 - magic_iceshield_sheet.png
 - magic_torrentacle.png
 
Joshua Taylor
 - Vegies.png
 
Radomir Dopieralski
 - whitey.png
 
Thane & Stacey Brimhall
 - button_default.png
 - button_small.png
 - confirm_bg.png
 - logo.png
 - slider_default.png
 